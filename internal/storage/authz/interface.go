package authz

import (
	"github.com/pkg/errors"
	"time"
)

const (
	AT_STORAGE_KEY = "at"
	RT_STORAGE_KEY = "rt"
)

var (
	NotFountErr = errors.New("not found key in storage")
)

type AuthzStorage interface {
	Add(string, interface{}, time.Duration) error
	Get(string) (interface{}, error)
}
