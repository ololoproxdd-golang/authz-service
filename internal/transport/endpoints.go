package transport

import (
	"context"
	"github.com/go-kit/kit/endpoint"
	"github.com/rs/zerolog/log"
	"gitlab.com/ololoproxdd-golang/authz-service/internal/service/authz"
	"gitlab.com/ololoproxdd-golang/authz-service/internal/service/health"
	v1 "gitlab.com/ololoproxdd-golang/authz-service/pkg/api/v1"
)

type Endpoints struct {
	Ping     endpoint.Endpoint
	Health   endpoint.Endpoint
	Create   endpoint.Endpoint
	Validate endpoint.Endpoint
}

func NewEndpoints(s authz.AuthzService, c health.HealthCheck) *Endpoints {
	return &Endpoints{
		Ping:     makePingEndpoint(c),
		Health:   makeHealthEndpoint(c),
		Create:   makeCreateEndpoint(s),
		Validate: makeValidateEndpoint(s),
	}
}

func makePingEndpoint(c health.HealthCheck) endpoint.Endpoint {
	return func(context.Context, interface{}) (interface{}, error) {
		return c.Ping()
	}
}

func makeHealthEndpoint(c health.HealthCheck) endpoint.Endpoint {
	return func(context.Context, interface{}) (interface{}, error) {
		return c.Health()
	}
}

func makeCreateEndpoint(s authz.AuthzService) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (interface{}, error) {
		req, ok := request.(v1.CreateJwtRequest)
		if !ok {
			log.Fatal().
				Interface("request", request).
				Msg("makeCreateEndpoint not type of v1.CreateJwtRequest")
		}

		return s.Create(req)
	}
}

func makeValidateEndpoint(s authz.AuthzService) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (interface{}, error) {
		req, ok := request.(v1.ValidateJwtRequest)
		if !ok {
			log.Fatal().
				Interface("request", request).
				Msg("makeValidateEndpoint not type of v1.ValidateJwtRequest")
		}

		return s.Validate(req)
	}
}
