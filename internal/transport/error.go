package transport

import (
	"context"
	"github.com/rs/zerolog/log"
)

type errorHandler struct {
	transport string
}

func NewErrorHandler(transport string) *errorHandler {
	return &errorHandler{transport}
}

func (h *errorHandler) Handle(ctx context.Context, err error) {
	log.Error().Err(err).Msgf("transport in \"%s\"", h.transport)
}
