package server

import (
	"github.com/go-redis/redis"
	"github.com/rs/zerolog/log"
	"gitlab.com/ololoproxdd-golang/authz-service/internal/config"
	"gitlab.com/ololoproxdd-golang/authz-service/internal/service/authz"
	"gitlab.com/ololoproxdd-golang/authz-service/internal/service/health"
	authz_storage "gitlab.com/ololoproxdd-golang/authz-service/internal/storage/authz"
	health_storage "gitlab.com/ololoproxdd-golang/authz-service/internal/storage/health"
	"gitlab.com/ololoproxdd-golang/authz-service/internal/transport"
	"google.golang.org/grpc"
	"sync"
)

type server struct {
	grpcServer *grpc.Server
	config     config.Config
	endpoints  *transport.Endpoints

	healthService health.HealthCheck
	authzService  authz.AuthzService

	authzStorage  authz_storage.AuthzStorage
	healthStorage health_storage.HealthStorage

	redisClient *redis.Client
}

func NewServer(config config.Config) *server {
	return &server{config: config}
}

// close all descriptors
func (s *server) Close() {
	if err := s.redisClient.Close(); err != nil {
		log.Info().Err(err).Msg("closing redis connect error")
	}

	s.grpcServer.Stop()
}

// open init structure
func (s *server) Open() error {
	s.initLogger()
	s.registerOsSignal()

	return nil
}

func (s *server) Run() {
	wg := sync.WaitGroup{}

	wg.Add(1)
	go s.runGrpcServer(&wg)

	wg.Wait()
	log.Info().Msg("authz-service stopped")
}
