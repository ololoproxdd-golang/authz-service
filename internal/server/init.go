package server

import (
	"github.com/go-redis/redis"
	"github.com/rs/zerolog/log"
	"gitlab.com/ololoproxdd-golang/authz-service/internal/service/authz"
	"gitlab.com/ololoproxdd-golang/authz-service/internal/service/health"
	authz_storage "gitlab.com/ololoproxdd-golang/authz-service/internal/storage/authz"
	health_storage "gitlab.com/ololoproxdd-golang/authz-service/internal/storage/health"
	"gitlab.com/ololoproxdd-golang/authz-service/internal/transport"
	v1 "gitlab.com/ololoproxdd-golang/authz-service/pkg/api/v1"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"time"
)

func (s *server) getRedisClient() *redis.Client {
	if s.redisClient != nil {
		return s.redisClient
	}

	s.redisClient = redis.NewClient(
		&redis.Options{
			Addr:            s.config.Redis,
			MaxRetries:      3,
			MaxRetryBackoff: 100 * time.Millisecond,
			ReadTimeout:     50 * time.Millisecond,
			WriteTimeout:    50 * time.Millisecond,

			PoolSize:     200,
			MinIdleConns: 80,
			PoolTimeout:  100 * time.Millisecond,
		},
	)

	if _, err := s.redisClient.Ping().Result(); err != nil {
		log.Fatal().Err(err).Msg("redis connection failed")
	}
	log.Info().Msg("created redis client")

	return s.redisClient
}

func (s *server) getAuthzStorage() authz_storage.AuthzStorage {
	if s.authzStorage != nil {
		return s.authzStorage
	}

	s.authzStorage = authz_storage.NewRedisStorage(s.config, s.getRedisClient())
	log.Info().Msg("created authz storage")

	return s.authzStorage
}

func (s *server) getHealthStorage() health_storage.HealthStorage {
	if s.healthStorage != nil {
		return s.healthStorage
	}

	s.healthStorage = health_storage.NewHealthStorage(s.getRedisClient())
	log.Info().Msg("created health storage")

	return s.healthStorage
}

func (s *server) getAuthzService() authz.AuthzService {
	if s.authzService != nil {
		return s.authzService
	}
	s.authzService = authz.NewAuthzService(s.config, s.getAuthzStorage())
	log.Info().Msgf("created authz service")

	return s.authzService
}

func (s *server) getHealthService() health.HealthCheck {
	if s.healthService != nil {
		return s.healthService
	}
	s.healthService = health.NewHealthService(s.getHealthStorage())
	log.Info().Msgf("created health service")

	return s.healthService
}

func (s *server) getEndpoints() *transport.Endpoints {
	if s.endpoints != nil {
		return s.endpoints
	}

	s.endpoints = transport.NewEndpoints(s.getAuthzService(), s.getHealthService())
	log.Info().Msgf("created server endpoints")
	return s.endpoints

}

func (s *server) getTransportGrpc() *grpc.Server {
	if s.grpcServer != nil {
		return s.grpcServer
	}
	s.grpcServer = grpc.NewServer(grpc.UnaryInterceptor(GrpcLoggerInterceptor))
	v1.RegisterAuthzServiceServer(s.grpcServer, transport.MakeGrpcServer(s.getEndpoints()))
	reflection.Register(s.grpcServer)

	log.Info().Msgf("created transport - grpc")

	return s.grpcServer
}
