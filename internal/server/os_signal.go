package server

import (
	"fmt"
	"github.com/rs/zerolog/log"
	"os"
	"os/signal"
	"syscall"
)

// register os signal terminate application
func (s *server) registerOsSignal() {
	osSignal := make(chan os.Signal, 1)
	signal.Notify(osSignal, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		defer close(osSignal)
		sig := <-osSignal
		log.Info().Msg(fmt.Sprintf("get signal: %s", sig))
		s.Close()
	}()
}
