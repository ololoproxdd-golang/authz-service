package config

import (
	"os"

	"github.com/jessevdk/go-flags"
	"github.com/joho/godotenv"
	"github.com/rs/zerolog/log"
)

func LoadConfig() Config {
	var config Config

	_ = godotenv.Load() // get variables from .env file

	var parser = flags.NewParser(&config, flags.Default)
	if _, err := parser.Parse(); err != nil {
		panic(err)
	}

	config.HostName = getHostName()

	return config
}

type Config struct {
	AppEnv string `short:"e" long:"app-env" env:"APP_ENV" description:"Application env name"`

	AppHost string `short:"h" long:"app-host" env:"APP_HOST" description:"Application server host"`
	AppPort string `short:"p" long:"app-port" env:"APP_PORT" description:"Application server port"`

	LogPath string `short:"l" long:"log-path" env:"LOG_PATH" description:"Filename for logs"`

	Redis string `short:"r" long:"redis" env:"REDIS_CONNECTION_STRING" description:"Redis connection string host:port"`

	JwtSecret           string `long:"jwt-secret" env:"JWT_SECRET" description:"Secret for encrypt"`
	AccessTokenExpTime  int    `long:"access-token-expiration-time" env:"ACCESS_TOKEN_EXPIRATION_TIME" description:"Access Token expired time"`
	RefreshTokenExpTime int    `long:"refresh-token-expiration-time" env:"REFRESH_TOKEN_EXPIRATION_TIME" description:"Refresh Token expired time"`

	HostName string
}

func (c *Config) IsStage() bool {
	return c.AppEnv == "stage"
}

func (c *Config) IsProd() bool {
	return c.AppEnv == "production"
}

func (c *Config) UseHeadlessServices() bool {
	return c.IsProd() || c.IsStage()
}

func (c *Config) UseSyslog() bool {
	return c.IsProd() || c.IsStage()
}

func getHostName() string {
	hostName, err := os.Hostname()
	if err != nil {
		log.Warn().Err(err).Msg("can`t get hostname")
	}
	return hostName
}
