package authz

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	"gitlab.com/ololoproxdd-golang/authz-service/internal/config"
	storage "gitlab.com/ololoproxdd-golang/authz-service/internal/storage/authz"
	v1 "gitlab.com/ololoproxdd-golang/authz-service/pkg/api/v1"
	"time"
)

type authzService struct {
	cfg          config.Config
	redisStorage storage.AuthzStorage
}

func NewAuthzService(cfg config.Config, redisStorage storage.AuthzStorage) *authzService {
	return &authzService{
		cfg:          cfg,
		redisStorage: redisStorage,
	}
}

func (s *authzService) Create(request v1.CreateJwtRequest) (*v1.CreateJwtResponse, error) {
	if request.AccessToken == "" {
		return nil, accessTokenNotFound
	}

	iat := time.Now()
	exp := iat.Add(time.Duration(s.cfg.AccessTokenExpTime) * time.Minute)
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"iss":          "authz-service",
		"sub":          "authorization",
		"iat":          iat.Unix(),
		"exp":          exp.Unix(),
		"company":      request.Company,
		"email":        request.Email,
		"access_token": request.AccessToken,
	})

	jwtToken, err := token.SignedString([]byte(s.cfg.JwtSecret))
	if err != nil {
		return nil, errors.Wrap(err, "signed string error")
	}

	//jwt : access_token
	key := fmt.Sprintf("%s:%s", storage.AT_STORAGE_KEY, jwtToken)
	err = s.redisStorage.Add(key, request.AccessToken, time.Duration(s.cfg.AccessTokenExpTime)*time.Minute)
	if err != nil {
		return nil, err
	}

	// jwt : refresh_token
	if request.RefreshToken != "" {
		key = fmt.Sprintf("%s:%s", storage.RT_STORAGE_KEY, jwtToken)
		err = s.redisStorage.Add(key, request.RefreshToken, time.Duration(s.cfg.RefreshTokenExpTime)*time.Minute)
		if err != nil {
			return nil, err
		}
	} else {
		log.Warn().Err(errors.New("Refresh token is empty. Can't add to storage"))
	}

	return &v1.CreateJwtResponse{JwtToken: jwtToken}, nil
}

func (s *authzService) Validate(request v1.ValidateJwtRequest) (*v1.ValidateJwtResponse, error) {
	token, err := jwt.Parse(request.JwtToken, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, errors.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		return []byte(s.cfg.JwtSecret), nil
	})

	if err != nil {
		return nil, errors.Wrap(err, "JWT parse error")
	}

	response := &v1.ValidateJwtResponse{}
	if _, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		response.IsValid = true
	} else {
		response.IsValid = false
	}

	return response, nil
}
