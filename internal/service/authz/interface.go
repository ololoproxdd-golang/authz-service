package authz

import (
	"github.com/pkg/errors"
	v1 "gitlab.com/ololoproxdd-golang/authz-service/pkg/api/v1"
)

var (
	accessTokenNotFound = errors.New("Access token is empty")
)

type AuthzService interface {
	Create(v1.CreateJwtRequest) (*v1.CreateJwtResponse, error)
	Validate(v1.ValidateJwtRequest) (*v1.ValidateJwtResponse, error)
}
