package health

import (
	v1 "gitlab.com/ololoproxdd-golang/authz-service/pkg/api/v1"
)

type HealthCheck interface {
	Ping() (*v1.PingResponse, error)
	Health() (*v1.HealthResponse, error)
}
